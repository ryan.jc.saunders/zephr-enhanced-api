# Import the required libraries
import hashlib
import random
import requests
from requests.exceptions import HTTPError
import csv
import time
import re
import json
from flask import Flask, jsonify, request

app = Flask(__name__)
app.json.sort_keys = False

@app.route('/')
def home():
    return 'Zephr Enhanced API - Status: Online'

@app.route('/rng')
def about():
    return jsonify({"random_number": random.randint(1, 6)})  # Simplified random number generation

@app.route('/api/account-domain-update', methods=['POST'])
def account_domain_update():

    success, response_json, response_code = account_domain_update_validation(request)

    if success:
        # Split payload into variables to be able to use them
        payload = request.json
        zephr_credentials = payload.get('zephr_credentials')
        account_id = payload.get('account_id')
        domain = payload.get('domain')
        action = payload.get('action')
        exemptions = payload.get('exemptions', [])

        # Function to make a call to download a CSV of all users
        def fetch_users_csv():
            baseURL = f"https://{zephr_credentials['tenant_id']}.api.zephr.com"
            path = "/v4/user-export"
            headers = updateHMAC("", path, "", "GET", zephr_credentials["zephr_access_key"], zephr_credentials["zephr_secret_key"])
            r = requests.request(method="GET", url=f"{baseURL}{path}", headers=headers)
            r.raise_for_status()  # Raise an exception for any HTTP errors
            return r.text

        # Function to make a call to fetch account users in JSON format
        def fetch_account_users_json(zephr_credentials, account_id):
            baseURL = f"https://{zephr_credentials['tenant_id']}.api.zephr.com"
            path = f"/v3/accounts/{account_id}/users"
            headers = updateHMAC("", path, "", "GET", zephr_credentials["zephr_access_key"], zephr_credentials["zephr_secret_key"])
            
            try:
                r = requests.request(method="GET", url=f"{baseURL}{path}", headers=headers)
                r.raise_for_status()  # Raise an exception for any HTTP errors
                return r.json()
            except HTTPError as e:
                if e.response.status_code == 404:
                    # Return an empty dictionary if the endpoint returns a 404 error
                    return {}
                else:
                    # Re-raise the exception if it's not a 404 error
                    raise

        # Function to filter out users by domain (works with CSV & JSON)
        def filter_users_by_domain(data, domain, data_format="csv"):
            users = []

            if data_format == "csv":
                reader = csv.DictReader(data.splitlines())
                for row in reader:
                    email = row.get("email")
                    if email and email.endswith(f"@{domain}"):
                        users.append({"user_id": row["user_id"], "email": email})
            elif data_format == "json":
                for user in data:
                    email = user.get("user_email")
                    if not email:
                        email = user.get("email")
                    if email and email.endswith(f"@{domain}"):
                        users.append({"user_id": user["user_id"], "email": email})
            else:
                raise ValueError("Unsupported data format. Supported formats are 'csv' and 'json'.")

            return users

        # Function to further filter users and remove any that match the exemptions list (or another list of users!)
        def filter_exemption_users(users, exemptions):
            filtered_users = []
            
            for user in users:
                user_id = user["user_id"]
                email = user["email"]
                match = False
                
                for exemption in exemptions:
                    identifier = exemption.get("identifier")
                    value = exemption.get("value")
                    
                    if identifier is not None:
                        # Handle exemptions with "identifier" key
                        if identifier == "user_id" and user_id == value:
                            match = True
                            break
                        elif identifier == "email_address" and email == value:
                            match = True
                            break
                    else:
                        # Handle cases where another user list has been passed in with 2 keys (user_id & email)
                        if user_id == exemption["user_id"] or email == exemption["email"]:
                            match = True
                            break

                if not match:
                    filtered_users.append(user)
            
            return filtered_users

        # Extract account users for the specified account which match the specified domain
        account_users = filter_users_by_domain(fetch_account_users_json(zephr_credentials, account_id), domain, "json")

        if "ADD" in action:
            users = filter_users_by_domain(fetch_users_csv(), domain, "csv")
            users = filter_exemption_users(users, account_users)
            users = filter_exemption_users(users, exemptions)

            users_added = 0 if "PREVIEW" in action else add_users(users, account_id, zephr_credentials)

            response_data = {
                "account_id": account_id,
                "domain": domain,
                "action": action,
                "users_to_add": len(users),
                "users_added": users_added,
                "users_failed": 0 if "PREVIEW" in action else len(users) - users_added,
                "users": users
            }

            return jsonify(response_data)

        elif "REMOVE" in action:
            account_users = filter_exemption_users(account_users, exemptions)

            users_removed = 0 if "PREVIEW" in action else remove_users(account_users, account_id, zephr_credentials)

            response_data = {
                "account_id": account_id,
                "domain": domain,
                "action": action,
                "users_to_remove": len(account_users),
                "users_removed": users_removed,
                "users_failed": 0 if "PREVIEW" in action else len(account_users) - users_removed,
                "users": account_users
            }

            return jsonify(response_data)

    else:
        # Respond with JSON response and appropriate status code
        return jsonify(response_json), response_code


# Endpoint for retrieving user sessions by user_id
@app.route('/api/user-sessions/<user_id>', methods=['GET', 'DELETE'])
def user_sessions(user_id):
    if request.method == 'GET':
        # Placeholder response for GET request
        return jsonify({"user_id": user_id}), 200
    elif request.method == 'DELETE':
        # Placeholder response for DELETE request
        return jsonify({"message": "Placeholder for DELETE request"}), 200


def add_users(users, account_id, zephr_credentials):
    users_added = 0
    for user in users:
        time.sleep(0.01)
        user_id = user["user_id"]
        baseURL = f"https://{zephr_credentials['tenant_id']}.api.zephr.com"
        path = f"/v4/accounts/{account_id}/users/{user_id}"
        body = '{"user_type": "user"}'
        headers = updateHMAC(body, path, "", "PUT", zephr_credentials["zephr_access_key"], zephr_credentials["zephr_secret_key"])
        try:
            response = requests.put(baseURL + path, headers=headers, data=body)
            response.raise_for_status()
            users_added += 1
        except requests.exceptions.RequestException as e:
            print(f"Failed to add user with ID {user_id}: {e}")
    return users_added

def remove_users(users, account_id, zephr_credentials):
    users_removed = 0
    for user in users:
        user_id = user["user_id"]
        baseURL = f"https://{zephr_credentials['tenant_id']}.api.zephr.com"
        path = f"/v4/accounts/{account_id}/users/{user_id}"
        headers = updateHMAC("", path, "", "DELETE", zephr_credentials["zephr_access_key"], zephr_credentials["zephr_secret_key"])
        try:
            response = requests.delete(baseURL + path, headers=headers)
            response.raise_for_status()
            users_removed += 1
        except requests.exceptions.RequestException as e:
            print(f"Failed to remove user with ID {user_id}: {e}")
    return users_removed

# Check if the auth details provided are valid
def zephr_auth_check(zephr_credentials):

    if not zephr_credentials or 'zephr_access_key' not in zephr_credentials or 'zephr_secret_key' not in zephr_credentials:
        return False, jsonify({"message": "Invalid authentication payload"}), 400

    access_key = zephr_credentials["zephr_access_key"]
    secret_key = zephr_credentials["zephr_secret_key"]
    tenant = zephr_credentials["tenant_id"]

    baseURL = f"https://{tenant}.api.zephr.com"
    path = "/v3/users"
    query = "rpp=1"
    headers = updateHMAC("", path, query, "GET", access_key, secret_key)
    r = requests.request(method="GET", url=f"{baseURL}{path}", params=query, headers=headers)
    
    if r.status_code == 200:
        return True, r.json(), 200
    else:
        return False, r.json(), 401

def account_domain_update_validation(request):
    if request.method != 'POST':
        return False, {"message": "Request type not supported"}, 405
        
    payload = request.json
    if not payload:
        return False, {"message": "Invalid payload"}, 400
        
    # Split payload into variables to be able to use them
    zephr_credentials = payload.get('zephr_credentials')
    account_id = payload.get('account_id')
    domain = payload.get('domain')
    action = payload.get('action')
    exemptions = payload.get('exemptions')
                
    # Make a call to zephr_auth_check with the credentials that were passed as part of the payload to this endpoint - This will check if they are valid credentials
    success, response_json, response_code = zephr_auth_check(zephr_credentials)

    if not success:
        return False, response_json, response_code

    # Validate the action
    if action not in ['ADD', 'REMOVE', 'PREVIEW_ADD', 'PREVIEW_REMOVE']:
        return False, {"message": "Invalid action in payload. Expected values are 'ADD', 'REMOVE', 'PREVIEW_ADD' or 'PREVIEW_REMOVE'"}, 400

    # Validate the domain
    if not is_valid_domain(domain):
        return False, {"message": "Invalid domain in payload. Please use a valid domain"}, 400

    # Check if the account exists
    if not account_exists(account_id, zephr_credentials):
        return False, {"message": "Account does not exist"}, 404
    
    # Validate exemptions
    if not validate_exemptions(exemptions):
        return False, {"message": "Invalid exemptions within payload. Ensure an identifier and value are present"}, 400
    
    return True, None, None

# Helper function to check if the domain in the payload is valid or not
def is_valid_domain(domain):
    # Regular expression to match a valid domain name
    domain_regex = r'^([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\.)+[a-zA-Z]{2,}$'
    
    # Check if the domain matches the regex pattern
    if re.match(domain_regex, domain):
        return True
    else:
        return False

# Helper function to check if the exemptions within the payload are valid and contain an identifier and a value
def validate_exemptions(exemptions):
    # If exemptions is an empty list, it's considered valid
    if not exemptions:
        return True

    # List of valid exemption identifiers
    valid_identifiers = {"email_address", "user_id"}

    # Iterate through each exemption object in the exemptions list
    for exemption in exemptions:
        # Check if the exemption object has both 'identifier' and 'value' keys
        if not isinstance(exemption, dict) or 'identifier' not in exemption or 'value' not in exemption:
            return False
        
        # Check if the identifier is valid
        if exemption['identifier'] not in valid_identifiers:
            return False

    # All exemptions are valid
    return True

# Helper function to check if the account is valid and exists
def account_exists(account_id, zephr_credentials):
    # Construct the base URL using the tenant ID
    baseURL = f"https://{zephr_credentials['tenant_id']}.api.zephr.com"
    
    # Endpoint for fetching account details
    path = f"/v3/accounts/{account_id}"
    
    # Generate HMAC headers
    headers = updateHMAC("", path, "", "GET", zephr_credentials["zephr_access_key"], zephr_credentials["zephr_secret_key"])
    
    # Make the GET request to check if the account exists
    r = requests.request(method="GET", url=f"{baseURL}{path}", headers=headers)
    
    # Check if the request was successful (status code 200)
    if r.status_code == 200:
        return True
    else:
        return False

# Function which builds the HMAC header based on requirements
def updateHMAC(body="", path="", query="", method="GET", accessKey="", secretKey=""):
    
    # Capture timestamp & random nonce value (Ensure Unix timestamp is 13 chars)
    timestamp = str(int(time.time_ns() / 1000))[0:13]
    nonce = str(random.random())

    # Build message string which will be converted to SHA256
    message = f"{secretKey}{body}{path}{query}{method}{timestamp}{nonce}"

    # Convert to hexed hash
    hashString = hashlib.sha256(message.encode()).hexdigest()

    # Define the Authorization header
    zephrAuthHeader = f"ZEPHR-HMAC-SHA256 {accessKey}:{timestamp}:{nonce}:{hashString}"

    headers = {
        'Authorization': zephrAuthHeader,
        'Content-Type': 'application/json'
    }
    
    return headers

if __name__ == '__main__':
    app.run()
