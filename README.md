# Zephr Enhanced API

The Zephr Enhanced API utilises the existing [Zephr Admin API](https://developer.zuora.com/api-references/zephr-admin-api/overview/) and enhances several endpoints, providing additional functionality which is currently missing from official Admin API.

[TOC]

---

## Account Domain Updates

**Description:**
Allows you to add or remove all users from an account using a specific domain as the identifier/filter.

**Endpoint:**
`/api/account-domain-update`

**Available Methods:**
`POST`

**Request Payload Type:**
`application/json`

**Request Payload Schema:**
- **account_id** : The ID of the account you wish to add or remove users from.
- **domain** : The domain you'd like to use to filter users (i.e. `zuora.com`).
- **action** : The action you'd like to take (i.e. add or remove users, or preview these actions). Valid actions are: `ADD`|`REMOVE`|`PREVIEW_ADD`|`PREVIEW_REMOVE`
- **exemptions** : You can list any users you'd like to take no action on (i.e. if you're removing users from an account but wish for some to remain, etc).
- **zephr_credentials** :
  - **tenant_id** : The ID of the tenant you're performing actions against
  - **zephr_access_key** : Your Zephr Access Key
  - **zephr_secret_key** : Your Zephr Secret Key

**Example Payload:**
```json
{
  "account_id": "63977b3d-eb0c-4452-8094-a1b704ff47e4",
  "domain": "zephr.com",
  "action": "ADD",
  "exemptions": [
    {"identifier": "email_address", "value": "ryan.saunders+test1@zephr.com"},
    {"identifier": "email_address", "value": "ryan.saunders+test2@zephr.com"},
    {"identifier": "user_id", "value": "fc7462e2-600a-4956-9cc9-5ebfc0d51b70"},
    {"identifier": "user_id", "value": "d11b561c-18af-40e5-a63e-6dcb85832830"}
  ],
  "zephr_credentials": {
    "tenant_id": "ryansaunders",
    "zephr_access_key": "my_access_key",
    "zephr_secret_key": "my_secret_key"
  }
}
```

**Example Response:**
```json
{
    "account_id": "63977b3d-eb0c-4452-8094-a1b704ff47e4",
    "domain": "zephr.com",
    "action": "ADD",
    "users_to_add": 2,
    "users_added": 2,
    "users_failed": 0,
    "users": [
        {
            "user_id": "10748e7c-b046-4b43-a5ce-a35e067ac5a0",
            "email": "ryan.saunders+test3@zephr.com"
        },
        {
            "user_id": "1748efbc-cbb4-42cf-82d9-11ce38d3dc1a",
            "email": "ryan.saunders+test4@zephr.com"
        }
    ]
}
```

## User Session Management

**Description:**
Allows you to manage active sessions for a specific user_id (i.e. delete all sessions).

**Endpoint:**
`/api/user-sessions/{user_id}`

**Available Methods:**
`GET` - View detailed information for all active sessions
`DELETE` - Delete all active sessions

**Request Path Schema:**
- **user_id** : The ID of the user you wish to view/delete sessions for.

**Example GET Response:**
```json
{
    "account_id": "63977b3d-eb0c-4452-8094-a1b704ff47e4",
    "domain": "zephr.com",
    "action": "ADD",
    "users_to_add": 2,
    "users_added": 2,
    "users_failed": 0,
    "users": [
        {
            "user_id": "10748e7c-b046-4b43-a5ce-a35e067ac5a0",
            "email": "ryan.saunders+test3@zephr.com"
        },
        {
            "user_id": "1748efbc-cbb4-42cf-82d9-11ce38d3dc1a",
            "email": "ryan.saunders+test4@zephr.com"
        }
    ]
}
```

**Example DELETE Response:**
```json
{
    "account_id": "63977b3d-eb0c-4452-8094-a1b704ff47e4",
    "domain": "zephr.com",
    "action": "ADD",
    "users_to_add": 2,
    "users_added": 2,
    "users_failed": 0,
    "users": [
        {
            "user_id": "10748e7c-b046-4b43-a5ce-a35e067ac5a0",
            "email": "ryan.saunders+test3@zephr.com"
        },
        {
            "user_id": "1748efbc-cbb4-42cf-82d9-11ce38d3dc1a",
            "email": "ryan.saunders+test4@zephr.com"
        }
    ]
}
```

## Enhanced Account Information

Third section content.

## Todo:

- [ ] enhanced account info (seats total, taken, free, etc)

- [ ] session enhancements - delete all sessions for a user, get all info, etc.